<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get("/files", [FileController::class, "index"])->name("files");

Route::get("/getUserFiles", [FileController::class, "getUserFiles"]);

Route::get("/fileDownload/{hash}", [FileController::class, "fileDownload"]);

Route::delete("/deleteFile", [FileController::class, "deleteFile"]);

Route::patch("/updateProtect/{id}", [FileController::class, "updateProtect"]);

Route::post('/saveFiles', [FileController::class, "saveFiles"]);

require __DIR__.'/auth.php';

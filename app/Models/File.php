<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use HasFactory, softDeletes;

    /**
     * Fillable columns in db table
     */
    protected $fillable = [
        "originalFilename",
        "fileHash",
        "size",
    ];

    /**
     * For soft delete
     */
    protected $dates = [
        "deleted_at",
    ];

    /**
     * File relations with users
     */
    public function users()
    {
        return $this->belongsToMany(User::class, "users_files", "file_id", "user_id");
    }
}

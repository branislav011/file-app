<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\File;
use App\Models\User;

class ProcessFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * list of uploaded files
     */
    protected $files;

    /**
     * user ID - auth user
     */
    protected $userID;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public function __construct(array $files, int $userID)
    {
        $this->files = $files;
        $this->userID = $userID;

        $this->onQueue("files");
    }

    /**
     * Execute the job. Assing files to user
     * @return void
     */
    public function handle(): void
    {
        $files = $this->files;
        $userID = $this->userID;
        $user = User::find($userID);
        $today = new \DateTime();
        $groupHash = md5($today->format("Y-m-d H:i:s") . $userID) . ".zip";

        foreach ($files as $hash => $file) {
            $ext = $file["ext"];
            $filename = $file["filename"];
            $size = $file["size"]; //size in bytes

            $newFile = File::create([
                "originalFilename" => $filename,
                "fileHash" => $hash,
                "size" => $size,
                "protected" => 0,
            ]);

            $fileID = $newFile->id;

            $zip = new \ZipArchive();
            $zipHash = $hash . ".zip";
            $zip->open(storage_path("app/public/userFiles/") . $zipHash, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            $zip->addFile(storage_path("app/public/userFiles/") . $hash, $hash);
            $zip->close();
            
            $user->files()->attach($userID, [
                "file_id" => $fileID, 
                "groupHash" => $groupHash,
                "created_at" => $today->format("Y-m-d H:i:s"), 
                "updated_at" => $today->format("Y-m-d H:i:s"),
            ]);
        }
    }
}

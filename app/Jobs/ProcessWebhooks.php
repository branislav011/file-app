<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Str;
use Spatie\WebhookServer\WebhookCall;
use Illuminate\Support\Facades\Http;

class ProcessWebhooks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * url list
     */
    protected $endpoints;

    /**
     * xml or json payload
     */
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $data, array $endpoints)
    {
        $this->data = $data;
        $this->endpoints = $endpoints;

        $this->onQueue("webhooks");
    }

    /**
     * Execute the job.
     * Send webhooks
     *
     * @return void
     */
    public function handle():void
    {
        $endpoints = $this->endpoints;
        $data = $this->data;
        $secret = Str::random(40);

        foreach ($endpoints as $endpoint) {
            //send post request

            //with library
            // WebhookCall::create()
            //     ->url($endpoint)
            //     ->payload($data)
            //     ->useSecret($secret)
            //     ->dispatch();

            //with http client
            Http::post($endpoint, [
                'payload' => $data,
            ]);
        }
    }
}

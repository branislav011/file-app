<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Arr;

use App\Models\User;
use App\Models\File;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * User can delete file
    */
    public function fileOwner(User $user, File $file): Response
    {
        $fileID = $file->id;
        $userFiles = $user->files->toArray();
        $userHasFile = Arr::first($userFiles, function($file, $key) use($fileID) {
            return $file["id"] == $fileID;
        });

        return !empty($userHasFile) ? Response::allow() : Response::deny("Not authorized.");
    }
}

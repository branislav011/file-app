<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use App\Payload\Interfaces\Payload;
use App\Payload\Classes\XMLPayload;
use App\Payload\Classes\JSONPayload;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Payload::class, function($app) {
            if (config("app.payload_type") == "json") {
                return new JSONPayload();
            }

            if (config("app.payload_type") == "xml") {
                return new XMLPayload();
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::share('zipPassword', config("app.zipPassword"));
    }
}

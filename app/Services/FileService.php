<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Models\User;
use App\Models\File;
use App\Jobs\ProcessFiles;

class FileService
{
    /**
     * Redis connection
     */
    protected $redis;

    public function __construct()
    {
        $this->redis = Redis::connection();
    }
    /**
     * List of user files
     */
    public function getUserFiles(): Collection
    {
        $userID = auth()->user()->id;
        $fileList = User::find($userID)->files;

        return $fileList;
    }

    /**
     * store user files and dispatch queue
     */
    public function storeFiles(array $files, array $newFileNames, int $userID): array
    {
        $today = new \DateTime();

        foreach ($files as $i => $file) {
            $ext = $file->getClientOriginalExtension();
            $newFileName = $newFileNames[$i];
            $filename = $newFileName . "." . $ext;
            $size = $file->getSize(); //size in bytes
            $hash = $file->hashName();

            $file->store("/public/userFiles");
            $fileData = [
                "filename" => $filename,
                "size" => $size,
                "ext" => $ext,
            ];

            $fileList[$hash] = $fileData;
            $fileData["hash"] = $hash;

            $this->redis->rpush("files", json_encode($fileData));
        }

        ProcessFiles::dispatch($fileList, $userID);

        return $fileList;
    }

    /**
     * Download file or zip
     */
    public function downloadFile(string $hash): BinaryFileResponse
    {
        $ext = pathinfo($hash, PATHINFO_EXTENSION);
        
        if ($ext == "zip") {
            $filename = pathinfo($hash, PATHINFO_FILENAME);
        } else {
            $filename = $hash;
        }

        $file = File::where("fileHash", $filename)->first();
        $path = storage_path("app/public/userFiles/") . $hash;

        $this->fileExists($path);

        $this->checkFileAuth($file);

        return response()->download($path);
    }

    /**
     * Mark for delete
     */
    public function deleteFile(int $fileID): bool
    {
        $file = File::find($fileID);
 
        $this->checkFileAuth($file);

        return $file->delete();//mark for delete
    }

    /**
     * Switch password protection
     */
    public function updateProtect(int $fileID): int
    {
        $file = File::find($fileID);

        $this->checkFileAuth($file);
        
        $protected = $file->protected;
        $protected = !$protected;

        $file->protected = $protected;
        $file->save();

        return $file->protected;
    }

    /**
     * Permanently delete files - cron
     * php artisan delete:files
     */
    public function deletePermanently(): bool
    {
        $files = File::onlyTrashed()->get();

        foreach ($files as $file) {
            $path = $file->fileHash;
            $zip = $path . ".zip";
            $filename = $file->originalFilename;
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $fileData = [
                "filename" => $filename,
                "size" => $file->size,
                "ext" => $ext,
                "hash" => $path,
            ];

            Storage::disk("publicFiles")->delete($path);
            Storage::disk("publicFiles")->delete($zip);

            $this->redis->rpush("deletedFiles", json_encode($fileData));
        }

        return File::onlyTrashed()->forceDelete();
    }

    /**
     * Redis backup - user files
     */
    public function getFilesRedisDB(): array
    {
        $data = $this->redis->lrange("files", 0, -1);

        return $data;
    }

    /**
     * Redis backup - deleted files
     */
    public function getDeletedFilesRedisDB(): array
    {
        $data = $this->redis->lrange("deletedFiles", 0, -1);

        return $data;
    }

    /**
     * Check if user can modify his own file
     */
    private function checkFileAuth(File $file): bool
    {
        $response = Gate::inspect("fileOwner", $file);

        if (!$response->allowed()) {
            abort(403, $response->message());
        }

        return true;
    }

    /**
     * Check if file exists in storage
     */
    private function fileExists(string $path): void
    {
        if (!file_exists($path)) {
            Log::warning("File not exists", [
                "path" => $path,
            ]);

            abort(404, "Not Found");
        }
    }
}

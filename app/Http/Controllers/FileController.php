<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreFilesRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Services\FileService;
use App\Payload\Interfaces\Payload;

class FileController extends Controller
{   
    /**
     * Service class for controller
     */
    protected $service;

    /**
     * payload
     */
    protected $payload;

    /**
     * Setup services
     */
    public function __construct(FileService $service, Payload $payload)
    {
        $this->middleware("auth");

        $this->service = $service;
        $this->payload = $payload;
    }

    /**
     * List of user files
     */
    public function index(): View
    {
        $fileList = $this->service->getUserFiles();

        return view("files.index", [
            "files" => $fileList,
        ]);
    }

    /**
     * Save files
     * dispatch queue job
     * Send webhooks
     */
    public function saveFiles(StoreFilesRequest $request): RedirectResponse
    {
        $files = $request->file("files");
        $newFileNames = $request->newFileNames;
        $userID = auth()->user()->id;
        $endpointList = config("app.endpoint_list");
        
        $fileList = $this->service->storeFiles($files, $newFileNames, $userID);
        
        $this->payload->send($endpointList, $fileList);

        return redirect()->back()->with(["success" => "Uspesno sacuvano"]);
    }

    /**
     * Download file
     */
    public function fileDownload(string $fileHash): BinaryFileResponse
    {
        return $this->service->downloadFile($fileHash);
    }

    /**
     * Mark for delete
     */
    public function deleteFile(Request $request): JsonResponse
    {
        $fileID = $request->fileID;

        $this->service->deleteFile($fileID);

        return response()->json([
            "message" => "Uspesno sacuvano",
        ], Response::HTTP_OK);
    }

    /**
     * Password protect
     */
    public function updateProtect(int $fileID): JsonResponse
    {
        $this->service->updateProtect($fileID);

        return response()->json([
            "message" => "Uspesno sacuvano",
        ], Response::HTTP_OK);
    }

    /**
     * Get user files
     * ajax - json
     */
    public function getUserFiles(): JsonResponse
    {
        $fileList = $this->service->getUserFiles();

        return response()->json([
            "files" => $fileList,
        ], Response::HTTP_OK);
    }
}

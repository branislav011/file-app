<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\File;
use App\Services\FileService;

class DeleteFiles extends Command
{
    /**
     * Service class for controller - files
     */
    protected $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete user files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FileService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->service->deletePermanently();
    }
}

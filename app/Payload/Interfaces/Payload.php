<?php

namespace App\Payload\Interfaces;

interface Payload
{
    public function send(array $endpoints, array $fileList): void;

    public function formatData(array $fileList): string;
}

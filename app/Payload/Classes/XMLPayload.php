<?php

namespace App\Payload\Classes;

use App\Payload\Interfaces;
use App\Jobs\ProcessWebhooks;

/**
 * XML payload for webhooks
 * Strategy Pattern
 */
class XMLPayload implements Payload
{
     /**
     * Dispatch job
     * Send HTTP Post request to webhooks
     */
    public function send(array $endpoints, array $fileList): void
    {
        $data = $this->formatData($fileList);

        ProcessWebhooks::dispatch($data, $endpoints);
    }

    /**
     * Generate XML
     */
    public function formatData(array $fileList): string
    {
        $doc = new DomDocument('1.0');
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $root = $doc->createElement('files');
        $root = $doc->appendChild($root);

        foreach ($fileList as $hash => $file) {
            $fileURL = "http://localhost/fileDownload/{$hash}";

            $fileElement = $doc->createElement('fileurl');
            $value = $doc->createTextNode($fileURL);
            $fileElement->appendChild($value);
            $root->appendChild($fileElement);
        }

        $xml = $doc->saveXML();

        return $xml;
    }
}

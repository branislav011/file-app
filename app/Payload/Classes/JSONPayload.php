<?php

namespace App\Payload\Classes;

use App\Payload\Interfaces\Payload;
use App\Jobs\ProcessWebhooks;

/**
 * JSON payload for webhooks
 * Strategy Pattern
 */
class JSONPayload implements Payload
{
    /**
     * Dispatch job
     * Send HTTP Post request to webhooks
     */
    public function send(array $endpoints, array $fileList): void
    {
        $data = $this->formatData($fileList);

        ProcessWebhooks::dispatch($data, $endpoints);
    }

    /**
     * Generate JSON
     */
    public function formatData(array $fileList): string
    {
        $json = [];

        foreach ($fileList as $hash => $file) {
            $fileURL = "http://localhost/fileDownload/{$hash}";

            $json[] = ["file_url" => $fileURL];
        }

        return json_encode($json);
    }
}

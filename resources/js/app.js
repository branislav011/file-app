import { createApp } from 'vue'

import FileComponent from './components/files/FileComponent.vue';
import FileListComponent from './components/files/FileListComponent.vue';
import FileUploadComponent from './components/files/FileUploadComponent.vue';
import FileSubmitComponent from './components/files/FileSubmitComponent.vue';

window.axios = require('axios');

const app = createApp({});

app.component('new-file', FileComponent);
app.component('file-list', FileListComponent);
app.component('file-upload', FileUploadComponent);
app.component('file-submit', FileSubmitComponent);

app.mount('#app');

require('./bootstrap');

require('alpinejs');

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Fajlovi') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 m-auto w-75">
                    @if ($errors->any())
                        <div class="alert alert-danger redBgd">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="greenBgd">
                            <div>
                                <p>
                                    {{ session()->get('success') }}
                                </p>
                            </div>
                        </div>
                    @endif

                    <div id="app">
                        <file-list password="{{ $zipPassword }}"></file-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 w-50 m-auto">
                    @if ($errors->any())
                        <div class="alert alert-danger redBgd">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="greenBgd">
                            <div class="col-lg-12">
                                <p>
                                    {{ session()->get('success') }}
                                    <span class="cursorPointer float-right" id="removeMsg">X</span>
                                </p>
                            </div>
                        </div>
                    @endif
                    <div id="app">
                        <form method="POST" action="/saveFiles" enctype="multipart/form-data">
                            @csrf
                            <new-file></new-file>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let msg = document.getElementById("removeMsg");

        if (msg) {
            msg.onclick = () => {
                document.querySelector(".greenBgd").style.display = "none";
            };
        }
    </script>
</x-app-layout>

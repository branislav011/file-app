Development tools:

1. PHP 8 
2. Laravel 8 
3. MySQL 8
4. Redis 6.2
5. Vue.js 3
6. PHPUnit 9.5
7. Docker 20.10
8. Linux / Ubuntu 20.04


Run project:

1. git clone https://branislav011@bitbucket.org/branislav011/file-app.git
2. cd file-app
3. cp .env.example .env
4. composer install
5. npm install
6. php artisan migrate
7. php artisan db:seed
8. ./vendor/bin/sail up
9. npm run dev
10. php artisan queue:work redis --queue=files,webhooks
11. Project available at: http://localhost/

Run tests:

1. /vendor/bin/phpunit

Routes:

1. File upload: http://localhost or http://localhost/dashboard
2. File list: http://localhost/files

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Queue;

use App\Models\User;
use App\Models\File;
use App\Jobs\ProcessFiles;
use App\Jobs\ProcessWebhooks;

class FileTest extends TestCase
{
    /**
     * Logged user can see form for uploading files
     */
    public function test_logged_user_can_see_form(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get("/dashboard");

        $response->assertSee("<new-file></new-file>", false);
    }

    /**
     * Logged user can save files to DB
     */
    public function test_logged_user_can_save_file(): void
    {
        $today = new \DateTime();
        $filename = "test.jpg";
        $filehash = md5($today->format("Y-m-d H:i:s") . $filename);

        $file = File::factory()->create([
            "originalFilename" => $filename,
            "fileHash" => $filehash,
            "size" => 0,
        ]);

        $this->assertDatabaseHas("files", ["originalFilename" => $filename]);
    }

    /**
     * ProcessFiles queue job dispatched
     */
    public function test_process_files_job_dispatched(): void
    {
        Queue::fake();

        $today = new \DateTime();
        $filename = "test.jpg";
        $filehash = md5($today->format("Y-m-d H:i:s") . $filename);

        $user = User::factory()->create();

        $fileData = [
            "filename" => $filename,
            "size" => 0,
            "ext" => ".jpg",
        ];

        $fileList[$filehash] = $fileData;

        ProcessFiles::dispatch($fileList, $user->id);

        Queue::assertPushed(ProcessFiles::class);
    }

    /**
     * ProcessWebhooks queue job dispatched
     */
    public function test_process_webhooks_job_dispatched(): void
    {
        Queue::fake();

        $data = "Payload...";
        $endpoints = [];

        ProcessWebhooks::dispatch($data, $endpoints);

        Queue::assertPushed(ProcessWebhooks::class);
    }
    
    /**
     * file upload - Logged user can upload file
     */
    public function test_logged_user_can_upload_file(): void
    {
        Storage::fake("publicFiles");

        $filename = "test.jpg";
        $file = UploadedFile::fake()->image($filename);
        $filehash = $file->hashName();
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post("/saveFiles", [
            "files" => $file,
        ]);

        $response->assertSee("Uspesno sacuvano");
    }

    /**
     * Download - no file
     */
    public function test_file_not_exists_404_returned(): void
    {
        $user = User::factory()->create();

        $filehash = "invalid";

        $response = $this->actingAs($user)->get("/fileDownload/$filehash");

        $response->assertNotFound();
    }

    /**
     * Delete file
     */
    public function test_logged_user_can_delete_file(): void
    {
        $user = User::factory()->create();
        $file = File::factory()->create();
        $fileID = $file->id;
        $userID = $user->id;
        $fileRel = File::factory()->userRelatedFile($userID, $fileID);

        $response = $this->actingAs($user)->delete("/deleteFile", [
            "fileID" => $fileID,
        ]);
        
        $file->refresh();

        $deleted = $file->deleted_at;

        $this->assertNotNull($deleted);
    }

    /**
     * Update protection
     */
    public function test_logged_user_can_update_protection(): void
    {
        $user = User::factory()->create();
        $file = File::factory()->create();
        $fileID = $file->id;
        $userID = $user->id;
        $fileRel = File::factory()->userRelatedFile($userID, $fileID);
        $protectedOld = $file->protected;

        $response = $this->actingAs($user)->patch("/updateProtect/$fileID");

        $file->refresh();

        $protectedNew = $file->protected;

        $this->assertNotEquals($protectedOld, $protectedNew);
    }

    /**
     * Test delete - artisan command
     */
    public function test_artisan_command_delete_files(): void
    {
        $this->artisan("delete:files")->assertExitCode(0);
    }

    /**
     * Logged user cant upload files - no files
     */
    public function test_logged_user_cannot_upload_empty_files_sent(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post("/saveFiles", [
            "files" => []
        ]);

        $response->assertSessionHasErrors(["files"]);
    }

    /**
     * Logged user can see uploaded files
     */
    public function test_logged_user_can_see_files(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get("/files");

        $response->assertSee("<file-list password", false);
    }
}

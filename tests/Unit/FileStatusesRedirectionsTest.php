<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Models\User;

class FileStatusesRedirectionsTest extends TestCase
{
    /**
     * Check status after visiting /files route
     */
    public function test_status_200_after_visit_route_files(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get("/files");

        $response->assertStatus(200);
    }

    /**
     * Check status after visiting / route
     */
    public function test_status_200_after_visit_home_route(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get("/");

        $response->assertStatus(200);
    }

    /**
     * Check status after visiting /dashboard route
     */
    public function test_status_200_after_visit_dashboard_route(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get("/dashboard");

        $response->assertStatus(200);
    }

    /**
     * Redirect if user not logged - route files
     */
    public function test_route_files_redirect_if_user_not_logged(): void
    {
        $response = $this->get("/files");

        $response->assertRedirect("/login");
    }

    /**
     * Redirect if user not logged - route /
     */
    public function test_route_home_redirect_if_user_not_logged(): void
    {
        $response = $this->get("/");

        $response->assertRedirect("/login");
    }

    /**
     * Redirect if user not logged - route dashboard
     */
    public function test_route_dashboard_redirect_if_user_not_logged(): void
    {
        $response = $this->get("/dashboard");

        $response->assertRedirect("/login");
    }

}

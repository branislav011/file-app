<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

use DB;

class FileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = File::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "originalFilename" => "test.jpg",
            "fileHash" => Str::random(30),
            "size" => mt_rand(0, 1000000),
        ];
    }

    /**
     * For testing
     *
     * @return int
     */
    public function userRelatedFile($userID, $fileID)
    {
        return DB::table("users_files")->insertGetId(["user_id" => $userID, "file_id" => $fileID, "groupHash" => "Test"]);
    }

    /**
     * For testing
     *
     * @return int
     */
    public function createFileSpecName($filename, $filehash)
    {
        return [
            "originalFilename" => $filename,
            "fileHash" => $filehash,
            "size" => mt_rand(0, 1000000),
        ];
    }
}
